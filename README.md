# mtg-edh-learner

First attempt at using Spark&#39;s MLLib to make suggestions when building an EDH deck.

The intention is to play around until I&#39;m at a point where I can begin to use more card data. I will write a client of some sort that either screen scrapes or downloads CSV files to get deck lists. That will utilize my mtg-api-client to get card text, type, color identity, and all of that will be fed into the ML algorithms to create a robust predictive model.

From there, I want to build a set of services on top of it and a front-end where users can interactively construct decks. The cards they pick will be fed back into the ML model so it&#39;s continuously trained.

Ambitious. Baby steps.